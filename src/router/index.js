import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/pages/login',
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => import('@/views/dashboard/Dashboard.vue'),
  },
  {
    path: '/loans',
    name: 'loans',
    component: () => import('@/views/loans/Loans.vue'),
    children: [
      {
        path: '',
        name: 'loan-dashboard',
        component: () => import('@/views/loans/ApplyForLoanCard.vue'),
      },
      {
        path: '/loans/apply',
        name: 'loan-apply',
        component: () => import('@/views/loans/LoanApplicationForm.vue'),
      }
  ]
  },
  {
    path: '/my-cards',
    name: 'mycards',
    component: () => import('@/views/my-cards/MyCards.vue'),
  },
  {
    path: '/settings',
    name: 'settings',
    component: () => import('@/views/settings/Settings.vue'),
  },
  {
    path: '/pages/login',
    name: 'pages-login',
    component: () => import('@/views/pages/Login.vue'),
    meta: {
      layout: 'blank',
    },
  },
  {
    path: '/error-404',
    name: 'error-404',
    component: () => import('@/views/Error.vue'),
    meta: {
      layout: 'blank',
    },
  },
  {
    path: '*',
    redirect: 'error-404',
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
